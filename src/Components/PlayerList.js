import React from "react";
import PropTypes from "prop-types";
import Player from "./Player";
import ScoreboardContext from "./Context";

const PlayerList = (props) => {
  return (
    <ScoreboardContext.Consumer>
      {(context) => (
        <React.Fragment>
          {context.players.map((player, index) => (
            <Player
              {...player}
              key={player.id.toString()}
              index={index}
              changeScore={props.changeScore}
              removePlayer={props.removePlayer}
            />
          ))}
        </React.Fragment>
      )}
    </ScoreboardContext.Consumer>
  );
};

PlayerList.propTypes = {
  changeScore: PropTypes.func.isRequired,
  removePlayer: PropTypes.func.isRequired,
};

export default PlayerList;
