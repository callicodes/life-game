import React from "react";
import Scoreboard from './Context';
import PropTypes from 'prop-types'

const Stats = () => {
  
  return (
    <Scoreboard.Consumer>
      {context => {
        const totalPlayers = context.length;
        const totalPoints = context.reduce((total, player) => {
          return total + player.score;
        }, 0);
        return (
          <table className="stats">
          <tbody>
            <tr>
              <td>Players:</td>
              <td>{totalPlayers}</td>
            </tr>
            <tr>
              <td>Total Points:</td>
              <td>{totalPoints}</td>
            </tr>
          </tbody>
        </table>
        )
      }}

    </Scoreboard.Consumer>

   
  );
};

Stats.propTypes = {
  players: PropTypes.arrayOf(PropTypes.shape({
    score: PropTypes.number
  }))
}
export default Stats;
