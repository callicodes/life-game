import "./App.css";

import ScoreboardContext from './Context'

import React from "react";
import Header from "./Header";
import Player from "./Player";
import AddPlayerForm from './AddPlayerForm'

class App extends React.Component {
  //Application State
  state = {
    players: [
      {
        name: "Coding",
        score: 0,
        id: 1,
      },
      {
        name: "Solving a problem at work",
        score: 0,
        id: 2,
      },
      {
        name: "Checking the calendar",
        score: 0,
        id: 3,
      },
      {
        name: "LinkedInLearning",
        score: 0,
        id: 4,
      },
      {
        name: "Updating The React Guild",
        score: 0,
        id: 5,
      },
      {
        name: "Cleaning emails",
        score: 0,
        id: 6,
      },
    ],
  };

  //player / task id counter
  prevPlayerId = 4;

  getHighScore = () => {
    const scores = this.state.players.map( p => p.score );
    const highScore = Math.max(...scores);
    if (highScore) {
      return highScore;
    } 
    return null;
  }

  handleScoreChange = (index, delta) => {
    this.setState(prevState => ({
      score: prevState.players[index].score += delta 
    }));
  };

  handleRemovePlayer = (id) => {
    this.setState( prevState => {
      return {
        players: prevState.players.filter(p => p.id !== id)
      };
    });
  }
  
  handleAddPlayer = (name) => {

    this.setState( prevState => {
      return {

      players: [
        ...prevState.players,
        {
          name,
          score: 0,
          id: this.prevPlayerId += 1
        }
      ]

     } })
  }
  
  render() {
   const highScore = this.getHighScore();
    return (
      <ScoreboardContext.Provider value={this.state.players}>
      <div className="scoreboard">
        <Header />

        {/*Players / Task List*/}

        {this.state.players.map((player, index) => (
          <Player
            name={player.name}
            score={player.score}
            id={player.id}
            key={player.id.toString()}
            index={index}
            removePlayer={this.handleRemovePlayer}
            changeScore={this.handleScoreChange}
            isHighScore={highScore === player.score}  
          />
        ))}

        <AddPlayerForm addPlayer={this.handleAddPlayer} />
      </div>
      </ScoreboardContext.Provider>
    );
  }
}




export default App;
